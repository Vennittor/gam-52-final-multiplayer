﻿using UnityEngine;
//using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class InteractCheck : MonoBehaviour 
{
	[SerializeField] private Collider pickupDetectionArea;

	[SerializeField] private List<Collider> detectedPickups = new List<Collider>();
	[SerializeField] private List<Collider> ignoredColliders = new List<Collider>();

	public void Ignore(Collider collider)
	{
		ignoredColliders.Add (collider);
	}

	public void Uningore(Collider collider)
	{
			if(ignoredColliders.Contains(collider))
				ignoredColliders.Remove (collider); 
	}

	void Start () 
	{
		if (pickupDetectionArea == null)
		{
			pickupDetectionArea = GetComponent<Collider>();
		}
	}


	void OnTriggerEnter(Collider other)
	{
//		if (!isLocalPlayer)
//			return;

		IInteractable ip = other.gameObject.GetComponent<IInteractable> ();

		if (ip != null)
			detectedPickups.Add (other);
	}

	void OnTriggerExit(Collider other)
	{
//		if (!isLocalPlayer)
//			return;

		if(detectedPickups.Contains(other) )
			detectedPickups.Remove (other);
	}

	public Collider NearestPickup()
	{
		float closestDistance = Mathf.Infinity;
		Collider closestPickup = null;

		foreach (Collider pickup in detectedPickups) 
		{
			if (ignoredColliders.Contains (pickup))
				continue;

			NetworkGun netGun = pickup.GetComponent<NetworkGun> ();
			if (netGun != null) 
			{
				if (netGun.equipped == true)
					continue;
			}
			
			float newDistance = Vector3.Distance(this.transform.position, pickup.transform.position);

			if( newDistance < closestDistance)
			{
				closestDistance = newDistance;
				closestPickup = pickup;
			}
		}

		return closestPickup;
	}
}
