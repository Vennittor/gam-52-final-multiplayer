﻿
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class FPSController : NetworkBehaviour 
{
    [SyncVar(hook = "hpUpdated")] public float hp = 20;
    public float maxHp = 100;
    public float speed = 6.0F;
	public float jumpSpeed = 8.0F;
	public float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;
	private CharacterController controller;

	[SerializeField] private Transform weaponSlot;
	//[SerializeField] private NetworkGun equippedWeapon;
	[SyncVar(hook = "EquipNew")] public GameObject equippedWeapon;
	private NetworkGun weaponToEquip;

	[SerializeField] InteractCheck interactCheck;


	void Start()
	{
		controller = GetComponent<CharacterController>();

		if (weaponSlot == null)
			weaponSlot = transform.FindChild ("Weapon Slot");
		if (interactCheck == null)
			interactCheck = GetComponentInChildren<InteractCheck> ();

        Renderer r = GetComponent<Renderer>();
        if (!isLocalPlayer)
        {
            r.sharedMaterial.color = Color.green;
            MouseLook m = GetComponent<MouseLook>();
            Destroy(m);
            m = GetComponentInChildren<MouseLook>();
            Destroy(m);
            AudioListener a = GetComponentInChildren<AudioListener>();
            Destroy(a);
            GUILayer g = GetComponentInChildren<GUILayer>();
            Destroy(g);
            FlareLayer f = GetComponentInChildren<FlareLayer>();
            Destroy(f);
            Camera c = GetComponentInChildren<Camera>();
            Destroy(c);
		}
        else
        { 
            r.sharedMaterial.color = Color.red;
        }

    }

	void Update()
	{
        if (!isLocalPlayer)
        {
            return;
        }
		if (controller.isGrounded) 
		{
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= speed;
			if (Input.GetButton("Jump"))
				moveDirection.y = jumpSpeed;

		}
		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move(moveDirection * Time.deltaTime);

		if (Input.GetButtonDown ("Fire1")) 
		{	
			if (equippedWeapon != null)
				equippedWeapon.GetComponent<NetworkGun>().TriggerDown ();
		}
		else if(Input.GetButton("Fire1"))
		{
			if (equippedWeapon != null)
				equippedWeapon.GetComponent<NetworkGun>().TriggerHeld();
		}

	// Interact //TODO this needs to be a Comman
		if (Input.GetKeyDown (KeyCode.E))
		{
			Interact ();
		}
	}


	private void Interact()
	{
		IInteractable ip = interactCheck.NearestPickup ().GetComponent<IInteractable> ();
		if (ip != null)
			ip.Interact (this);
	}
		
	public void Pickup(GameObject pickup)
	{
		if (pickup == null)
			return;

		NetworkGun netGun = pickup.GetComponentInChildren<NetworkGun> ();

		if (netGun != null)
		{
			CmdEquipNew (netGun.gameObject);
		}
		else
		{
			//TODO add other Behaviours for non NetworkGun Items
			return;
		}

	}

	[Command]
	public void CmdEquipNew(GameObject newWeapon)
	{
		EquipNew (newWeapon);
	}

	public void EquipNew(GameObject newWeapon)
	{
		if(equippedWeapon != null)
			DropWeapon ();
		if(newWeapon != null)
			EquipWeapon (newWeapon.GetComponent<NetworkGun>());
	}

	public void EquipWeapon(NetworkGun weapon)
	{
		weapon.transform.position = weaponSlot.position;
		weapon.transform.rotation = weaponSlot.rotation;
		weapon.transform.SetParent (weaponSlot);

		interactCheck.Ignore(weapon.GetComponent<Collider>());

		weapon.GetComponent<Collider> ().enabled = false;
		weapon.GetComponent<Rigidbody> ().isKinematic = true;

		equippedWeapon = weapon.gameObject;
		weapon.equipped = true;
	}

	private void DropWeapon()
	{
		if (equippedWeapon == null)
			return;
			
		equippedWeapon.transform.SetParent (null);

		interactCheck.Uningore (equippedWeapon.GetComponent<Collider>());
		
		equippedWeapon.GetComponent<Collider> ().enabled = true;
		equippedWeapon.GetComponent<Rigidbody> ().isKinematic = false;

		equippedWeapon.GetComponent<NetworkGun> ().equipped = false;


	}

    public void hpUpdated(float newHp)
    {
        hp = newHp;
    }

    public void TakeDamage(float damage)
    {
        if (!isServer)
            return;

        hp -= damage;
        if (hp <= 0)
        {
			CmdEquipNew (null);

            Vector3 pos = new Vector3(Random.Range(10, 40), transform.position.y + 20, Random.Range(10, 40));
            hp = maxHp;
            RpcRespawn(pos);
        }
    }

    [ClientRpc]
    void RpcRespawn(Vector3 position)
    {
        if(isLocalPlayer)
            transform.position = position;
    }

}




