﻿
public interface IInteractable 
{
	void Interact(FPSController interactingController = null);
}
