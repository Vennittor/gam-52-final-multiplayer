﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkGun : NetworkBehaviour, IInteractable
{
	public enum FireMode {SINGLE, BURST, AUTOMATIC, AUTOMATICBURST};

	public string weaponName;
	private bool isEquipped = false;

	public GameObject projectile;
	public float projectileSpeed = 40;

	public FireMode firingMode = FireMode.SINGLE;

    public Transform firePoint;

	public float fireRate = 450;	//Firing Actions per Minute
	float nextFire = 0;

	public int burstSize = 1;
	public float burstRate = 600;	//Firing Rate within a Burst Action, used only with FireMode.BURST

	private Coroutine burstCoroutine = null;

	public bool equipped 
	{
		get{ return isEquipped;}
		set{ isEquipped = value;}
	}

	void Start ()
    {
		nextFire = 60 / fireRate;

		if (firePoint == null)
			Debug.LogError (this.name + " needs a Fire Point to be assigned");
	}

    void Update()
    {

	}

	public void TriggerDown()
	{
		if (Time.time >= nextFire && !(firingMode == FireMode.AUTOMATIC || firingMode == FireMode.AUTOMATICBURST) ) 
		{
			if (firingMode == FireMode.SINGLE) 
			{
				FireOnce ();
			}
			else if( firingMode == FireMode.BURST && burstCoroutine == null)
			{
				FireBurst ();
			}
		}
		else if (Time.time >= nextFire && (firingMode == FireMode.AUTOMATIC || firingMode == FireMode.AUTOMATICBURST) )
		{
			if (firingMode == FireMode.AUTOMATIC)
				FireOnce ();
			else if (firingMode == FireMode.AUTOMATICBURST && burstCoroutine == null)
				FireBurst ();
		}
	}

	public void TriggerHeld()
	{
		if (Time.time >= nextFire && (firingMode == FireMode.AUTOMATIC || firingMode == FireMode.AUTOMATICBURST) ) 
		{
			if (firingMode == FireMode.AUTOMATIC)
				FireOnce ();
			else if (firingMode == FireMode.AUTOMATICBURST && burstCoroutine == null)
				FireBurst ();
		}
	}

	void FireOnce()
	{
		nextFire = Time.time + (60 / fireRate);

		CmdFire(firePoint.position, firePoint.rotation, firePoint.transform.forward);

	}

	void FireBurst()
	{
		nextFire = Time.time + (60 / fireRate);

		burstCoroutine = StartCoroutine (BurstFireRoutine ());
	}

	IEnumerator BurstFireRoutine()
	{
		for (int burstCount = 1; burstCount <= burstSize ; burstCount++) 
		{	
			CmdFire(firePoint.position, firePoint.rotation, firePoint.transform.forward);

			yield return new WaitForSeconds (60 / burstRate);
		}

		burstCoroutine = null;
	}

	// |=====IInteractable Functions=====|
	public virtual void Interact(FPSController interactingController = null)
	{
		if(!equipped)
			EquipTo (interactingController);
	}

	private void EquipTo(FPSController interactingController)
	{
		if (interactingController != null)
			interactingController.Pickup (this.gameObject);
	}
		
	[Command]
    public void CmdFire(Vector3 pos, Quaternion rot, Vector3 forward)
	{	Debug.Log ("CmdFire()");
        if (!isServer)
            return;
        GameObject p = (GameObject)Instantiate(projectile, pos, rot);
        Rigidbody r = p.GetComponent<Rigidbody>();
		r.velocity = forward * projectileSpeed;

        Collider a = p.GetComponent<Collider>();
        Collider b = GetComponent<Collider>();
        Physics.IgnoreCollision(a, b, true);
        NetworkServer.Spawn(p);
    }
}
